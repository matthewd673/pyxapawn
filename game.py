import eel

eel.init("web")

@eel.expose
def moveTo(pawn, coord):
	print(pawn, coord)

def renderBoard():
	eel.setBoard(board[0], board[1], board[2])

board = [[0 for i in range(3)] for j in range(3) ]

print(board)

#set initial board
for i in range(3):
	board[i][2] = i + 1 #friendlies at bottom
	board[i][0] = i + 4 #enemies along top

#render initial board
renderBoard()

#start of game loop
turnCt = 1
won = False
winner = 0

#enter turn loop (max of 8 turns)
for i in range(8):
	if won == False:
		if turnCt % 2 == 0: #even, so cpu turn
			eel.printWeb("(" + str(turnCt) + ") CPU turn")
		else:
			eel.printWeb("(" + str(turnCt) + ") player turn")
		turnCt += 1

eel.start("game.html")